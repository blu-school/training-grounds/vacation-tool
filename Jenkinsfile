pipeline {
    agent { dockerfile true }
    environment {
        SERVICE_CREDS = credentials('runscrumqaadmin')
        VERSION = VersionNumber([
          versionNumberString : '${BUILD_YEAR}.${BUILD_MONTH}.${BUILD_DAY}-${BUILDS_TODAY}',
          projectStartDate : '2021-01-01'
        ])
        NODE_OPTIONS = '--max-old-space-size=8192'
        HOME = '.'
    }
    triggers {        
       // poll repo every minute for changes
      pollSCM('* * * * *')       
    }
    
    parameters {
      string(name: 'TENANT', defaultValue: 'https://runscrumqa.sharepoint.com', description: 'Which SharePoint tenant should be logged in to?')
      string(name: 'SITE', defaultValue: 'https://runscrumqa.sharepoint.com/sites/vacationtool', description: 'Which SharePoint site should be deployed to?')
    }
    stages {
        stage('Pre-Check') {
          steps {
            dir('frontend'){
              sh 'node --version'
              sh 'pwd'
            }
          }
        }
        
        
        stage('npm') {
          steps {
              dir('frontend'){
                sh 'npm i'
                sh 'npm rebuild node-sass'
              }
          }
        }

        stage('sonar') {
          steps {
            withSonarQubeEnv('sonar'){
              sh 'sonar-scanner'
            }
          }
        }

          stage('Quality Gate') {
            steps{
            // sleep(60)
               waitForQualityGate abortPipeline: true
           }
        }

        stage('build') {
          steps {
            dir('frontend') {
              sh "npm version $VERSION --no-git-tag-version"
              sh 'npm run ship'
            }
          }
        }
        
        stage('spo login') {
          steps {
            sh "m365 login --authType password --userName $SERVICE_CREDS_USR --password $SERVICE_CREDS_PSW"
          }
        }

        stage('deploy') {
          steps {
            script {
              def solutionId = sh(script: 'm365 spo app add -p /var/lib/jenkins/workspace/VacationTool/frontend/sharepoint/solution/vacation-tool.sppkg --overwrite', returnStdout: true).trim()

              sh("m365 spo app deploy --id ${solutionId} ")

              sh("m365 spo app upgrade --id ${solutionId} --siteUrl ${params.SITE}")
            }
          }
        }
    }

    post {
        always {
      deleteDir()
        }
        failure {
          mail to: '3e5e40ad.blubito.com@emea.teams.ms',
              subject: "Vacation tool failed: ${currentBuild.fullDisplayName}",
              body: "Build failed ${env.BUILD_URL}"
        }
    }
}
