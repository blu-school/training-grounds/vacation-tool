'use strict';

const build = require('@microsoft/sp-build-web');

build.addSuppression(`Warning - [sass] The local CSS class 'ms-Grid' is not camelCase and will not be type-safe.`);

// disable tslint
build.tslintCmd.enabled = false;

var eslint = require('gulp-eslint');
var updateGulpfile = function (build) {
  console.log('here');
  // create the subtask
  var eslintSubTask = build.subTask('eslint', function (gulp) {
    return gulp
      .src(['src/**/*.{ts,tsx}'])
      .pipe(eslint('./config/eslint.json'))
      .pipe(eslint.format())
      .pipe(eslint.failAfterError());
  });
  // register the task as part of the pre-build process
  build.rig.addPreBuildTask(build.task('eslint', eslintSubTask));
};

// add eslint
updateGulpfile(build);

build.initialize(require('gulp'));
