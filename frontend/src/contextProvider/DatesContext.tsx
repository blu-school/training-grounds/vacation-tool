import React, { createContext, useContext } from 'react';
import { createDatesStore } from '../stores/DatesStore';
import { useLocalObservable } from 'mobx-react';
import { IDatesStoreProps } from '../stores/IDatesStoreProps';

const DatesContext = createContext(null);

export const DatesProvider = ({ children }: { children: JSX.Element }): JSX.Element => {
  const datesStore = useLocalObservable(createDatesStore);

  return <DatesContext.Provider value={datesStore}> {children} </DatesContext.Provider>;
};

export const useDatesStore = (): IDatesStoreProps => useContext(DatesContext);
