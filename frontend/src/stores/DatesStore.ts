import { IDropdownOption } from 'office-ui-fabric-react';
import strings from 'VacationToolWebPartStrings';
import { CalendarMode } from '../webparts/vacationTool/shared/constants/DateTimeUtils';
import { IDatesStoreProps } from './IDatesStoreProps';

export const createDatesStore = (): IDatesStoreProps => {
  return {
    activeStep: 0,
    selectedDates: null,
    selectedRange: null,
    duration: 0,
    steps: [
      { name: 'Type', value: null, label: null },
      { name: 'Dates', value: null, label: null },
      { name: 'Approval', value: null, label: null },
      { name: 'Details', value: null, label: null }
    ],
    selectedTimeOption: {
      key: CalendarMode.range,
      text: strings.dateRange
    },
    //table which determines if an element is selected (first value) or blurred (second value) for all three components
    cssTable: [
      [0, 0],
      [0, 0],
      [0, 0]
    ],
    setActiveStep(idx: number) {
      this.activeStep = idx;
    },
    setSelectedDates(selectedDates) {
      this.selectedDates = selectedDates;
    },
    setSelectedRange(selectedRange) {
      this.selectedRange = selectedRange;
    },
    setSteps(steps) {
      this.steps = steps;
    },
    setDuration(duration) {
      this.duration = duration;
    },
    setCssTable(cssTable) {
      this.cssTable = cssTable;
    },
    setSelectedTimeOption(item){
      this.selectedTimeOption = item;
    }
  };
};
