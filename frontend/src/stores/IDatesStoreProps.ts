import { IDropdownOption } from "office-ui-fabric-react";

export interface IDatesStoreProps {
    activeStep: number;
    selectedDates: Date[] | null;
    selectedRange: Date[] | null;
    duration: number;
    selectedTimeOption: IDropdownOption;
    steps: { name: string; value: any; label: string }[];
    cssTable: number[][];
    setActiveStep: (idx: number) => void;
    setSelectedDates: (selectedDates: Date[]) => void;
    setSelectedRange: (selectedDates: Date[]) => void;
    setDuration: (value: number) => void;
    setSteps: (steps: any) => void;
    setCssTable: (cssTable: any) => void;
    setSelectedTimeOption: (item: IDropdownOption) => any;
  }