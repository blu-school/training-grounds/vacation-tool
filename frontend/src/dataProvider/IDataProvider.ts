export interface IDataProvider {
  getAllDates(): Promise<Date[]>;
}
