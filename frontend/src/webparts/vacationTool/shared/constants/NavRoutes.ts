export const NavRoutes = {
  HOME: '/',
  SIGN_IN: '/sign-in',
  REQUEST_VACATION: '/request-vacation',
  MY_VACATIONS: '/my-vacations',
  APPROVALS: '/approvals'
};
