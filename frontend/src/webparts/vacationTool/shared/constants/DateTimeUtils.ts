import moment from 'moment';

export const formatDate = (date: Date): string => moment(date).format('DD.MM.YYYY');

export const calcDateDuration = (dateArr: Date[]): number => {
  let count = 0;
  const curDate = new Date(dateArr[0].getTime());

  while (curDate <= dateArr[1]) {
    const dayOfWeek = curDate.getDay();
    if (dayOfWeek !== 0 && dayOfWeek !== 6) count++;
    curDate.setDate(curDate.getDate() + 1);
  }

  return count;
};

export enum CalendarMode {
  range = 'rangeMode',
  multiDate = 'multiDateMode'
}

export const dateAlreadyClicked = (dates: Date[], date: Date): boolean =>
  dates ? dates.some((d) => date.getTime() === d.getTime()) : false;
export const datesExcept = (dates: Date[], date: Date): Date[] =>
  dates ? dates.filter((d) => date.getTime() !== d.getTime()) : [];
