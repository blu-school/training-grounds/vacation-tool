declare interface IVacationToolWebPartStrings {
  ConfigurationLabel: string;
  ExperimentalFeatureFieldLabel: string;
  landingPageFirstHeader: string;
  landingPageSecondHeader: string;
  landingPageSubheader: string;
  requestVacationBtnLabel: string;
  header: string;
  subheader: string;
  errorMessage: string;
  firstNamePlaceholder: string;
  surnamePlaceholder: string;
  familyNamePlaceholder: string;
  idNumberPlaceholder: string;
  firstNameLabel: string;
  surnameLabel: string;
  familyNameLabel: string;
  idNumberLabel: string;
  submit: string;
  paidLeave: string;
  unpaidLeave: string;
  other: string;
  startDate: string;
  selectedDate: string;
  selectedDates: string;
  endDate: string;
  duration: string;
  timeDistribution: string;
  timeDistributionString: string;
  vacationApproved: string;
  vacationDeclined: string;
  vacationViewMsg: string;
  approvalViewMsg: string;
  dateRange: string;
  multipleDates: string;
  days: string;
  day: string;
  paidLeave: string;
  unpaidLeave: string;
  other: string;
}

declare module "VacationToolWebPartStrings" {
  const strings: IVacationToolWebPartStrings;
  export = strings;
}
