import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import { IPropertyPaneConfiguration, PropertyPaneCheckbox } from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart, WebPartContext } from '@microsoft/sp-webpart-base';
import * as strings from 'VacationToolWebPartStrings';
import { VacationTool } from './components/VacationTool';
import { IVacationToolProps } from './components/IVacationToolProps';
import { IDataProvider } from '../../dataProvider/IDataProvider';

export interface IVacationToolWebPartProps {
  isExperimentalFeature: boolean;
  context: WebPartContext;
  dataProvider: IDataProvider;
}

export default class VacationToolWebPart extends BaseClientSideWebPart<IVacationToolWebPartProps> {
  private _dataProvider: IDataProvider;

  public render(): void {
    const element: React.ReactElement<IVacationToolProps> = React.createElement(VacationTool, {
      isExperimentalFeature: this.properties.isExperimentalFeature,
      dataProvider: this._dataProvider,
      context: this.context,
      strings: strings
    });

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.ConfigurationLabel
          },
          groups: [
            {
              groupFields: [
                PropertyPaneCheckbox('areExperimentalFeaturesEnabled', {
                  checked: this.properties.isExperimentalFeature,
                  text: strings.ExperimentalFeatureFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
