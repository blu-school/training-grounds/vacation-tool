export interface Step {
  name: string;
  value: string;
}
