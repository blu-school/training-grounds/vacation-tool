import * as React from 'react';
import * as strings from 'VacationToolWebPartStrings';

const Approval: React.FC<{}> = () => {
  return (
    <>
      <h1>{strings.approvalViewMsg}</h1>
    </>
  );
};

export default Approval;
