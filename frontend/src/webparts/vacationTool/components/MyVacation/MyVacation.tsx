import * as React from 'react';
import * as strings from 'VacationToolWebPartStrings';

const MyVacation: React.FC<{}> = () => {
  return (
    <>
      <h1>{strings.vacationViewMsg}</h1>
    </>
  );
};

export default MyVacation;
