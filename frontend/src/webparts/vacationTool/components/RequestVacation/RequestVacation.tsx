import * as React from 'react';
import ProgressBar from '../ProgressBar/ProgressBar';
import VacationType from '../VacationType/VacationType';
import styles from './RequestVacation.module.scss';
import CalendarPage from '../CalendarPage/CalendarPage';
import { useDatesStore } from '../../../../contextProvider/DatesContext';
import { observer } from 'mobx-react';

const RequestVacation: React.FC = observer(() => {
  const { steps, setSteps, activeStep, setActiveStep } = useDatesStore();

  const clickableNextStep = !!steps[activeStep].value;
  const clickablePreviousStep = activeStep !== 0;

  const onSelect = (selection: string, stepId: number): void => {
    const newSteps = [...steps];
    newSteps[stepId].value = selection;
    setSteps(newSteps);
  };

  return (
    <>
      <ProgressBar />
      {activeStep === 0 ? <VacationType stepId={0} onSelect={onSelect} /> : null}
      {activeStep === 1 ? <CalendarPage stepId={1} onSelect={onSelect} /> : null}
      <div className={styles.requestVacation__navButtonsContainer}>
        <button
          onClick={() => setActiveStep(activeStep - 1)}
          disabled={!clickablePreviousStep}
          className={styles.requestVacation__navButtons}
        >
          <img
            className={styles.requestVacation__navButtons__img}
            src={require('../../../../images/arrow-left.png')}
            alt='other'
          />
          Back
        </button>

        <button
          onClick={() => setActiveStep(activeStep + 1)}
          disabled={!clickableNextStep}
          className={styles.requestVacation__navButtons}
        >
          {!clickableNextStep ? (
            <>
              <span className={styles.grey}>Next</span>
              <img
                className={styles.requestVacation__navButtons__img}
                src={require('../../../../images/arrow-right.png')}
                alt='other'
              />
            </>
          ) : (
            <>
              <span className={styles.lightBlue}>Next</span>
              <img
                className={styles.requestVacation__navButtons__img}
                src={require('../../../../images/arrow-right-lightblue.svg')}
                alt='other'
              />
            </>
          )}
        </button>
      </div>
    </>
  );
});

export default RequestVacation;
