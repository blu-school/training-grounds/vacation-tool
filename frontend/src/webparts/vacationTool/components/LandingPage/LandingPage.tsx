import * as React from 'react';
import { useHistory } from 'react-router-dom';
import strings from 'VacationToolWebPartStrings';
import { NavRoutes } from '../../shared/constants/NavRoutes';
import styles from './LandingPage.module.scss';

const LandingPage: React.FC = () => {
  const history = useHistory();

  return (
    <div className={styles.landingPage}>
      <div className={styles.landingPage__imgContainer}>
        <section>
          <span className={styles.left__cloud}></span>
          <span className={styles.right__cloud}></span>
          <img className={styles.landingPage__img} src={require('../../../../images/Blubus.svg')} alt='Blubus' />
        </section>
      </div>
      <div className={styles.landingPage__headersButtonsContainer}>
        <h2 className={styles.landingPage__header}>{strings.landingPageFirstHeader}</h2>
        <h3 className={styles.landingPage__header__bold}>{strings.landingPageSecondHeader}</h3>
        <h4 className={styles.landingPage__subheader}>{strings.landingPageSubheader}</h4>
        <button className={styles.landingPage__button} onClick={() => history.push(NavRoutes.REQUEST_VACATION)}>
          {strings.requestVacationBtnLabel}
        </button>
      </div>
    </div>
  );
};

export default LandingPage;
