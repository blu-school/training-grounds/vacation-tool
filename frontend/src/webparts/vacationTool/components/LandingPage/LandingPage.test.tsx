import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import LandingPage from './LandingPage';
import { BrowserRouter } from 'react-router-dom';

describe('LandingPage', () => {
  // to run the test comment out the LandingPage:16 -> <img> import cannot be resolved
  beforeEach(() => {
    render(
      <BrowserRouter>
        <LandingPage />
      </BrowserRouter>
    );
  });

  test('click request a vacation button', () => {
    // screen.debug(null, 20000);
    // if the requestVacationFn is passed as a prop
    fireEvent.click(screen.getByText('requestVacationBtnLabel'));
    // expect(requestVacationFn).toHaveBeenCalled();
  });
});
