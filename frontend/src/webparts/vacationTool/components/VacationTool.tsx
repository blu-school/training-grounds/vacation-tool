import * as React from 'react';
import { HashRouter, Switch, Route } from 'react-router-dom';
import { IVacationToolProps } from './IVacationToolProps';
import Navbar from './Navbar/Navbar';
import LandingPage from './LandingPage/LandingPage';
import MyVacation from './MyVacation/MyVacation';
import Approval from './Approval/Approval';
import { NavRoutes } from '../shared/constants/NavRoutes';
import RequestVacation from './RequestVacation/RequestVacation';
import { FunctionComponent } from 'react';
import { DatesProvider } from '../../../contextProvider/DatesContext';
import { Observer } from 'mobx-react';

export const VacationTool: FunctionComponent<IVacationToolProps> = (props: IVacationToolProps): JSX.Element => {
  return (
    <DatesProvider>
      <HashRouter>
        <Observer>
          {() => (
            <>
              <Navbar />
              <Switch>
                <Route exact path={NavRoutes.HOME} component={LandingPage} />
                <Route exact path={NavRoutes.REQUEST_VACATION} component={RequestVacation} />
                <Route exact path={NavRoutes.MY_VACATIONS} component={MyVacation} />
                <Route exact path={NavRoutes.APPROVALS} component={Approval} />
              </Switch>
            </>
          )}
        </Observer>
      </HashRouter>
    </DatesProvider>
  );
};
