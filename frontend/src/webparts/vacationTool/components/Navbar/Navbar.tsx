import * as React from 'react';
import styles from './Navbar.module.scss';
import * as strings from 'VacationToolWebPartStrings';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { NavRoutes } from '../../shared/constants/NavRoutes';

const Navbar: React.FC<{}> = () => {
  const [isNotifyTabOpen, setNotifyTab] = useState<boolean>(false);

  const openNotifyTab = (): void => {
    setNotifyTab(!isNotifyTabOpen);
  };

  const showNotifyTab = (): JSX.Element => {
    return (
      <>
        <img src={require('../../../../images/icon-notifications.svg')} alt='Notification icon' />
        <div className={styles.dropdown_notify}>
          <ul className={styles.dropdown_notify_content}>
            <li>{strings.vacationApproved}</li>
            <li>{strings.vacationDeclined}</li>
          </ul>
        </div>
      </>
    );
  };

  return (
    <>
      <div className={styles.navbar_container}>
        <div className={styles.navbar_left_hand_side_container}>
          <Link to={NavRoutes.HOME} className={styles.navbar__vt_logo}>
            <img className={styles.navbar__vt_logo} src={require('../../../../images/vt-logo.svg')} alt='VT logo' />
          </Link>
          <Link to={NavRoutes.HOME} className={styles.navbar_left_hand_side_container}>
            <h3 className={styles.navbar_title}>Vacation Tool</h3>
          </Link>
        </div>
        <div className={styles.navbar_right_hand_side_container}>
          <Link to={NavRoutes.MY_VACATIONS} className={styles.navbar_left_hand_side_container}>
            <h3 className={styles.navbar_title}>My Vacations</h3>
          </Link>
          <Link to={NavRoutes.APPROVALS} className={styles.navbar_left_hand_side_container}>
            <h3 className={styles.navbar_title}>Approvals</h3>
          </Link>
        </div>
        <div className={styles.navbar__notify_logo} onClick={() => openNotifyTab()}>
          {isNotifyTabOpen ? (
            showNotifyTab()
          ) : (
            <img
              // className={styles.icon}
              src={require('../../../../images/icon-notifications.svg')}
              alt='Notification icon'
            />
          )}
        </div>
      </div>
    </>
  );
};

export default Navbar;
