// export interface IDropdownOptions {
//   key: 'rangeMode' | 'multiDateMode';
//   text: string;
// }

export interface ICalendarPageProps {
  stepId: number;
  onSelect: (selection: string, stepId: number) => void;
}
