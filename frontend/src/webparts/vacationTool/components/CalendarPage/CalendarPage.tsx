import { css, Dropdown, IDropdownOption } from 'office-ui-fabric-react';
import * as React from 'react';
import { useEffect } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import * as strings from 'VacationToolWebPartStrings';
import useCheckMobileScreen from '../../hooks/useCheckMobileScreen';
import styles from './CalendarPage.module.scss';
import { ICalendarPageProps } from './ICalendarPageProps';
import { initializeIcons } from 'office-ui-fabric-react/lib/Icons';
import {
  calcDateDuration,
  CalendarMode,
  dateAlreadyClicked,
  datesExcept,
  formatDate
} from '../../shared/constants/DateTimeUtils';
import { observer } from 'mobx-react';
import { useDatesStore } from '../../../../contextProvider/DatesContext';

initializeIcons();

const CalendarPage: React.FC<ICalendarPageProps> = observer(({ stepId, onSelect }: ICalendarPageProps) => {
  const {
    selectedTimeOption,
    selectedDates,
    selectedRange,
    duration,
    setDuration,
    setSelectedDates,
    setSelectedRange,
    setSelectedTimeOption
  } = useDatesStore();
  const isMobile = useCheckMobileScreen();
  const timeOptions: IDropdownOption[] = [
    { key: CalendarMode.range, text: strings.dateRange },
    { key: CalendarMode.multiDate, text: strings.multipleDates }
  ];

  useEffect(() => {
    setDuration(0);
    setSelectedDates(selectedDates || null);
    setSelectedRange(selectedRange || null);
    onSelect(null, stepId);
  }, [selectedTimeOption]);

  useEffect(() => {
    if (selectedDates && selectedDates.length > 0) {
      setDuration(selectedDates.length);
      onSelect(selectedDates.map((d) => formatDate(d)).join(', '), stepId);
    }
  }, [selectedDates]);

  useEffect(() => {
    if (selectedRange && selectedRange[0] && selectedRange[1]) {
      setDuration(calcDateDuration(selectedRange));
      onSelect(`${formatDate(selectedRange[0])} - ${formatDate(selectedRange[1])}`, stepId);
    }
  }, [selectedRange]);

  const tileClassName = ({ date }): string[] => {
    const classNames = [styles.dayTile];
    if (dateAlreadyClicked(selectedDates, date)) {
      return [styles.activeDay, ...classNames];
    } else {
      return classNames;
    }
  };

  const addDateToMultiDate = (date: Date): void => {
    if (dateAlreadyClicked(selectedDates, date)) {
      setSelectedDates(datesExcept(selectedDates, date));
    } else if (selectedDates) {
      setSelectedDates([...selectedDates, date]);
    } else {
      setSelectedDates([date]);
    }
  };

  const LeftArrow = (): JSX.Element => {
    return <img src={require('../../../../../../frontend/src/images/CalendarLeftArrow.png')} alt='leftArrow' />;
  };
  const RightArrow = (): JSX.Element => {
    return <img src={require('../../../../../../frontend/src/images/CalendarRightArrow.png')} alt='rightArrow' />;
  };
  const tileDisabled = ({ date, view }): boolean => view === 'month' && (date.getDay() === 0 || date.getDay() === 6);

  return (
    <div className={css(styles.calendarPage__wrapper, isMobile && styles.calendarPage__wrapper__isMobile)}>
      <div
        className={css(
          styles.calendarPage__calendarWrapper,
          isMobile && styles.calendarPage__calendarWrapper__isMobile
        )}
      >
        {selectedTimeOption.key === 'rangeMode' ? (
          <Calendar
            selectRange={true}
            minDate={new Date()}
            showDoubleView={!isMobile}
            prevLabel={<LeftArrow />}
            nextLabel={<RightArrow />}
            value={selectedRange}
            onChange={(nextDate: Date[]) => setSelectedRange(nextDate)}
            tileDisabled={tileDisabled}
          />
        ) : null}
        {selectedTimeOption.key === 'multiDateMode' ? (
          <Calendar
            selectRange={false}
            minDate={new Date()}
            showDoubleView={!isMobile}
            prevLabel={<LeftArrow />}
            nextLabel={<RightArrow />}
            tileClassName={tileClassName}
            onClickDay={addDateToMultiDate}
            tileDisabled={tileDisabled}
          />
        ) : null}
      </div>
      <div
        className={css(styles.calendarPage__previewWrapper, isMobile && styles.calendarPage__previewWrapper__isMobile)}
      >
        <TimeOptionSelection
          timeOptions={timeOptions}
          selectedTimeOption={selectedTimeOption}
          isMobile={isMobile}
          setSelectedTimeOption={setSelectedTimeOption}
          setSelectedDates={setSelectedDates}
        />

        {selectedTimeOption.key === CalendarMode.range ? (
          <RangeSelectionPreview selectedRange={selectedRange} duration={duration} />
        ) : null}

        {selectedTimeOption.key === CalendarMode.multiDate ? (
          <MultiDateSelectionPreview selectedDates={selectedDates} duration={duration} />
        ) : null}
      </div>
    </div>
  );
});
export default CalendarPage;

const RangeSelectionPreview = ({
  selectedRange,
  duration
}: {
  selectedRange: Date[];
  duration: number;
}): JSX.Element => {
  return (
    <div className={styles.calendarPage__preview_data}>
      <label>{strings.startDate}</label>
      {selectedRange && selectedRange[0] ? formatDate(selectedRange[0]) : '-'}
      <label>{strings.endDate}</label>
      {selectedRange && selectedRange[1] ? formatDate(selectedRange[1]) : '-'}
      <label>{strings.duration}</label>
      {selectedRange ? (duration > 1 ? `${duration} ${strings.days}` : `${duration} ${strings.day}`) : '-'}
    </div>
  );
};

const MultiDateSelectionPreview = ({
  selectedDates,
  duration
}: {
  selectedDates: Date[];
  duration: number;
}): JSX.Element => {
  return (
    <div className={styles.calendarPage__preview_data}>
      <label>{strings.selectedDates}</label>
      {selectedDates && selectedDates.length > 0 ? (
        <ol>
          {selectedDates.map((d: Date, idx: number) => (
            <li key={idx}>{formatDate(d)}</li>
          ))}
        </ol>
      ) : (
        '-'
      )}
      <label>{strings.duration}</label>
      {selectedDates ? (duration > 1 ? `${duration} ${strings.days}` : `${duration} ${strings.day}`) : '-'}
    </div>
  );
};

const TimeOptionSelection = ({
  timeOptions,
  selectedTimeOption,
  isMobile,
  setSelectedTimeOption
}: {
  timeOptions: IDropdownOption[];
  selectedTimeOption: IDropdownOption;
  isMobile: boolean;
  setSelectedTimeOption: (option: IDropdownOption) => void;
  setSelectedDates: (dates: Date[] | null) => void;
}): JSX.Element => {
  const onSelectTimeOption = (_event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
    setSelectedTimeOption(item);
  };

  return (
    <div className={css(styles.datePickerContainer, isMobile && styles.datePickerContainer__isMobile)}>
      <img src={require('../../../../../../frontend/src/images/distribution.png')} alt='' />
      <div className={styles.distributionContainer}>
        <p>{strings.timeDistribution}</p>
        <Dropdown
          options={timeOptions}
          placeholder={strings.timeDistributionString}
          selectedKey={selectedTimeOption ? selectedTimeOption.key : undefined}
          onChange={onSelectTimeOption}
        />
      </div>
    </div>
  );
};
