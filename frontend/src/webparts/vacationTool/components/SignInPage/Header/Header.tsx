import IHeaderProps from './IHeaderProps';
import * as React from 'react';

const Header: React.FC<IHeaderProps> = ({ header, subheader }: IHeaderProps) => {
  return (
    <div>
      <img src={require('../../../../../images/Logo.svg')} alt='VacationLogo' />
      <p>
        <h3>{header}</h3>
        <h1>{subheader}</h1>
      </p>
    </div>
  );
};

export default Header;
