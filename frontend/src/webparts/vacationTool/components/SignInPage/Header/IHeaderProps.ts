export default interface IHeaderProps {
  header: string;
  subheader: string;
}
