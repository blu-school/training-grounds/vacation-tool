import * as React from 'react';
import Header from './Header/Header';
import SignInForm from './SignInForm/SignInForm';
import styles from './SignIn.module.scss';
import * as strings from 'VacationToolWebPartStrings';

const SignIn: React.FC = () => {
  return (
    <div className={styles.wrapper}>
      <Header header={strings.header} subheader={strings.subheader} />
      <SignInForm />
    </div>
  );
};

export default SignIn;
