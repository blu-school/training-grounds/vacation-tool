import * as React from 'react';
import Input from './Input/Input';
import styles from './Input/Input.module.scss';
import { SignInFormFields } from '../../../shared/constants/SignInFormFields';
import * as strings from 'VacationToolWebPartStrings';

const SignInForm: React.FC = () => {
  const [form, setForm] = React.useState({});
  const [isFieldError, setFieldIsError] = React.useState<{ name: string; isError: boolean }>({
    name: '',
    isError: false
  });
  const normalBorderColor = { borderColor: '#BCD0E1' };
  const [borderMyStyle, setborderStyle] = React.useState(normalBorderColor);

  const validate = (name: string, value: string): boolean => {
    switch (name) {
      case 'firstName':
      case 'surname':
      case 'familyName':
        if (!new RegExp(/^([А-Я]{1}[а-я]{1,})$/).test(value)) {
          setFieldIsError({ name: name, isError: true });
        } else {
          setFieldIsError({ name: name, isError: false });
        }
        break;
      case 'idNumber':
        if (!new RegExp(/^([0-9]{10})$/).test(value)) {
          setFieldIsError({ name: name, isError: true });
        } else {
          setFieldIsError({ name: name, isError: false });
        }
        break;
    }
    return isFieldError.isError;
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const name: string = event.target.name;
    const value: string = event.target.value;
    if (validate(name, value)) {
      setborderStyle({ borderColor: '#F82838' });
    } else {
      setborderStyle(normalBorderColor);
    }
    setForm((values) => ({
      ...values,
      [name]: value
    }));
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    const name = event.currentTarget.name;
    const value = event.currentTarget.value;
    event.preventDefault();
    validate(name, value);
    alert(JSON.stringify(form));
  };

  return (
    <>
      <div className={styles.inputLabel}>
        <form onSubmit={handleSubmit}>
          <Input
            name={SignInFormFields.FirstName}
            mystyle={
              isFieldError.name === SignInFormFields.FirstName && isFieldError.isError
                ? borderMyStyle
                : normalBorderColor
            }
            value={form[SignInFormFields.FirstName]}
            type='text'
            inputLabel={strings.firstNameLabel}
            placeholder={strings.familyNamePlaceholder}
            handleChange={handleChange}
            isRequired={true}
          />
          <Input
            mystyle={
              isFieldError.name === SignInFormFields.Surname && isFieldError.isError ? borderMyStyle : normalBorderColor
            }
            name={SignInFormFields.Surname}
            value={form[SignInFormFields.Surname]}
            type='text'
            inputLabel={strings.surnameLabel}
            placeholder={strings.surnamePlaceholder}
            handleChange={handleChange}
            isRequired={false}
          />
          <Input
            mystyle={
              isFieldError.name === SignInFormFields.FamilyName && isFieldError.isError
                ? borderMyStyle
                : normalBorderColor
            }
            name={SignInFormFields.FamilyName}
            value={form[SignInFormFields.FamilyName]}
            type='text'
            inputLabel={strings.firstNameLabel}
            placeholder={strings.firstNamePlaceholder}
            handleChange={handleChange}
            isRequired={true}
          />
          <Input
            mystyle={
              isFieldError.name === SignInFormFields.Id && isFieldError.isError ? borderMyStyle : normalBorderColor
            }
            name={SignInFormFields.Id}
            value={form[SignInFormFields.Id]}
            type='text'
            inputLabel={strings.idNumberLabel}
            placeholder={strings.idNumberPlaceholder}
            handleChange={handleChange}
            isRequired={true}
          />
          {isFieldError.isError && isFieldError.name ? (
            <div id='errorMessage'>
              <img src={require('./../../../../../images/warning.svg')} alt='warning' />
              <p>{strings.errorMessage}</p>{' '}
            </div>
          ) : (
            <div id='errorMessage'></div>
          )}

          <button type='submit' disabled={isFieldError.isError}>
            <img src={require('./../../../../../images/check.svg')} alt='check' />
            <p>{strings.submit}</p>
          </button>
        </form>
      </div>
    </>
  );
};

export default SignInForm;
