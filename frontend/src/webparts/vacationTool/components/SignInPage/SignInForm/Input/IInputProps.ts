import { ChangeEvent } from 'react';

export interface IInputProps {
  name: string;
  value: string;
  type: string;
  inputLabel: string;
  placeholder: string;
  isRequired: boolean;
  handleChange: (e: ChangeEvent) => void;
  mystyle: {};
}
