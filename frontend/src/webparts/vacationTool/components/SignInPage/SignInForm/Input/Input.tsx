import * as React from 'react';
import { IInputProps } from './IInputProps';
import styles from './Input.module.scss';

const Input: React.FC<IInputProps> = ({
  name,
  value,
  type,
  inputLabel,
  placeholder,
  isRequired,
  handleChange,
  mystyle
}: IInputProps) => {
  return (
    <>
      <label className={styles.inputLabel}>
        {inputLabel} <br />
        <input
          className={styles.inputField}
          style={mystyle}
          placeholder={placeholder}
          type={type}
          name={name}
          value={value}
          required={isRequired}
          onChange={handleChange}
        />
      </label>
    </>
  );
};

export default Input;
