export interface IVacationTypeProps {
  stepId: number;
  onSelect: (selection: string, stepId: number) => void;
}
