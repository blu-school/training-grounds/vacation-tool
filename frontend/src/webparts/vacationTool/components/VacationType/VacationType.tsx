import * as React from 'react';
import VacationOptionCard from './VacationOptionCard/VacationOptionCard';
import styles from './VacationType.module.scss';
import * as strings from 'VacationToolWebPartStrings';
import { IVacationTypeProps } from './IVacationTypeProps';
import { useDatesStore } from '../../../../contextProvider/DatesContext';

const VacationType: React.FC<IVacationTypeProps> = ({ stepId, onSelect }: IVacationTypeProps) => {
  const { cssTable, setCssTable } = useDatesStore();

  const vacationTypes = [strings.paidLeave, strings.unpaidLeave, strings.other];
  const clickHandler = (id: number): void => {
    if (id === 0) {
      setCssTable([
        [1, 0],
        [0, 1],
        [0, 1]
      ]);
    }
    if (id === 1) {
      setCssTable([
        [0, 1],
        [1, 0],
        [0, 1]
      ]);
    }
    if (id === 2) {
      setCssTable([
        [0, 1],
        [0, 1],
        [1, 0]
      ]);
    }
    onSelect(vacationTypes[id], stepId);
  };

  return (
    <div className={styles.vacationType_wrapper}>
      <div className={styles.vacationType__leaveOptions}>
        <VacationOptionCard
          id={0}
          imgSource={require('../../../../images/paid-leave.png')}
          text={strings.paidLeave}
          selected={!!+cssTable[0][0]} // "+" sign converts the number to string, "!!" sign converts string to boolean
          blurredOut={!!+cssTable[0][1]}
          clickHandler={() => clickHandler(0)}
        />
        <VacationOptionCard
          id={1}
          imgSource={require('../../../../images/unpaid-leave.png')}
          text={strings.unpaidLeave}
          selected={!!+cssTable[1][0]}
          blurredOut={!!+cssTable[1][1]}
          clickHandler={() => clickHandler(1)}
        />
        <VacationOptionCard
          id={2}
          imgSource={require('../../../../images/other.png')}
          text={strings.other}
          selected={!!+cssTable[2][0]}
          blurredOut={!!+cssTable[2][1]}
          clickHandler={() => clickHandler(2)}
        />
      </div>
    </div>
  );
};

export default VacationType;
