import { css } from 'office-ui-fabric-react';
import * as React from 'react';
import { IVacationOptionCardProps } from './IVacationOptionCardProps';
import styles from './VacationOptionCard.module.scss';

const VacationOptionCard: React.FC<IVacationOptionCardProps> = ({
  id,
  imgSource,
  text,
  selected,
  blurredOut,
  clickHandler
}: IVacationOptionCardProps) => {
  return (
    <div
      className={css(styles.vacationOptionCard, selected && 'selected', blurredOut && 'blurredOut')}
      onClick={() => clickHandler(id)}
    >
      <img className={styles.vacationOptionCard__img} src={imgSource} alt='paid-leave' />
      <span className={styles.vacationOptionCard__text}>{text}</span>
    </div>
  );
};

export default VacationOptionCard;
