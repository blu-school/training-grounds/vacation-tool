export interface IVacationOptionCardProps {
  id: number;
  imgSource: string;
  text: string;
  selected: boolean;
  blurredOut: boolean;
  clickHandler: (id: number) => void;
}
