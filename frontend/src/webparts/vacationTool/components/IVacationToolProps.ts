import { WebPartContext } from '@microsoft/sp-webpart-base';
import { IDataProvider } from '../../../dataProvider/IDataProvider';

export interface IVacationToolProps {
  isExperimentalFeature: boolean;
  strings: IVacationToolWebPartStrings;
  dataProvider: IDataProvider;
  context: WebPartContext;
}
