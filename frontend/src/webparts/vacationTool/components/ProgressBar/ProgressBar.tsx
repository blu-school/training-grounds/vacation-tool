import { observer } from 'mobx-react';
import { css } from 'office-ui-fabric-react';
import * as React from 'react';
import { useDatesStore } from '../../../../contextProvider/DatesContext';
import { Step } from '../../models/IStep.model';
import styles from './ProgressBar.module.scss';

/**
 * Lists all available steps to be completed.
 */
const ProgressBar: React.FC = observer(() => {
  const { steps, activeStep, setActiveStep } = useDatesStore();

  const sectionCheck = <img src={require('../../../../images/SectionCheck.svg')} alt='SectionCheck' />;
  const sectionActive = <img src={require('../../../../images/SectionActive.svg')} alt='SectionCheck' />;
  const sectionInactive = <img src={require('../../../../images/SectionInactive.svg')} alt='SectionCheck' />;

  const clickableStep = (idx: number): boolean => {
    return idx === 0 || (steps[idx - 1] && !!steps[idx - 1].value);
  };
  const onClickStep = (idx: number): void => {
    if (clickableStep(idx)) {
      setActiveStep(idx);
    }
  };

  return (
    <div className={styles.progress_bar_container}>
      <ul className={styles.progress_bar}>
        {steps.length
          ? steps.map((step: Step, idx: number) => {
              return (
                <li key={'step-' + idx} onClick={() => onClickStep(idx)}>
                  {activeStep === idx && !step.value && sectionActive}
                  {activeStep !== idx && !step.value && sectionInactive}
                  {step.value && sectionCheck}
                  <div className={styles.progress_bar_step_container}>
                    <div
                      className={css(
                        styles.progress_bar_step_name,
                        (activeStep === idx || step.value) && styles.active_black
                      )}
                    >
                      {step.name}
                    </div>
                    <div
                      className={css(
                        styles.progress_bar_step_value,
                        (activeStep === idx || step.value) && styles.active_black
                      )}
                    >
                      {step && step.value ? step.value : '-'}
                    </div>
                  </div>
                </li>
              );
            })
          : null}
      </ul>
    </div>
  );
});

export default ProgressBar;
