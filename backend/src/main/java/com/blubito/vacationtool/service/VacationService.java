package com.blubito.vacationtool.service;

import com.blubito.vacationtool.service.dto.VacationDTO;

import java.util.List;

/**
 * The interface Vacation service.
 */
public interface VacationService {

    /**
     * Gets all vacations.
     *
     * @return list of all vacations
     */
    List<VacationDTO> getAllVacationDays();

    /**
     * Save vacation dto.
     *
     * @param vacationDTO the vacation dto
     * @return the vacation dto
     */
    VacationDTO save(VacationDTO vacationDTO);
}
