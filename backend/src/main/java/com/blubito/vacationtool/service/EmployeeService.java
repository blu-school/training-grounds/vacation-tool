package com.blubito.vacationtool.service;

import com.blubito.vacationtool.service.dto.EmployeeDTO;

import java.util.List;

/**
 * The interface Employee service.
 */
public interface EmployeeService {

    /**
     * Get all employees.
     *
     * @return list of all employees.
     */
    List<EmployeeDTO> getAllEmployees();

    /**
     * Save employee dto.
     *
     * @param employeeDTO the employee dto
     * @return the created employee dto
     */
    EmployeeDTO save(EmployeeDTO employeeDTO);
}
