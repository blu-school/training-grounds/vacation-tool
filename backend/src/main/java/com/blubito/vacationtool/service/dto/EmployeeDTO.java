package com.blubito.vacationtool.service.dto;

import lombok.Data;

/**
 * The type Employee dto.
 */
@Data
public class EmployeeDTO {

    private Long id;

    private Long blubitoAccId;

    private String firstName;

    private String lastName;

    private String middleName;

    private String personalNumber;

}
