package com.blubito.vacationtool.service.impl;

import com.blubito.vacationtool.domain.Vacation;
import com.blubito.vacationtool.domain.enumeration.VacationStatus;
import com.blubito.vacationtool.repository.VacationRepository;
import com.blubito.vacationtool.service.VacationService;
import com.blubito.vacationtool.service.dto.VacationDTO;
import com.blubito.vacationtool.service.mapper.VacationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * The type Vacation service.
 */
@Service
@Transactional
public class VacationServiceImpl implements VacationService {

    private final Logger log = LoggerFactory.getLogger(VacationServiceImpl.class);

    /**
     * The Vacation repository.
     */
    public final VacationRepository vacationRepository;
    /**
     * The Vacation mapper.
     */
    public final VacationMapper vacationMapper;

    /**
     * Instantiates a new Vacation service.
     *
     * @param vacationRepository the vacation repository
     * @param vacationMapper     the vacation mapper
     */
    public VacationServiceImpl(VacationRepository vacationRepository, VacationMapper vacationMapper) {
        this.vacationRepository = vacationRepository;
        this.vacationMapper = vacationMapper;
    }

    @Override
    public List<VacationDTO> getAllVacationDays() {
        log.debug("Request to find all Vacations.");
        return vacationMapper.toDto(vacationRepository.findAll());
    }

    @Override
    public VacationDTO save(VacationDTO vacationDTO) {
        log.debug("Request to save Vacation: {}", vacationDTO);
        Vacation vacation = vacationMapper.toEntity(vacationDTO);
        vacation = vacationRepository.save(vacation);
        return vacationMapper.toDto(vacation);
    }
}

