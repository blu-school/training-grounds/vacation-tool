package com.blubito.vacationtool.service.impl;

import com.blubito.vacationtool.domain.Employee;
import com.blubito.vacationtool.repository.EmployeeRepository;
import com.blubito.vacationtool.service.EmployeeService;
import com.blubito.vacationtool.service.dto.EmployeeDTO;
import com.blubito.vacationtool.service.mapper.EmployeeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Employee service.
 */
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    private final Logger log = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    private final EmployeeRepository employeeRepository;

    private final EmployeeMapper employeeMapper;

    /**
     * Instantiates a new Employee service.
     *
     * @param employeeRepository the employee repository
     * @param employeeMapper     the employee mapper
     */
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
    }

    @Override
    public List<EmployeeDTO> getAllEmployees() {
        log.debug("Get all employees.");
        return employeeRepository.findAll()
                .stream()
                .map(employeeMapper::toDto)
                .collect(Collectors.toUnmodifiableList());
    }

    @Override
    public EmployeeDTO save(EmployeeDTO employeeDTO) {
        log.debug("Save an employee.");
        Employee employee = employeeMapper.toEntity(employeeDTO);
        employee = employeeRepository.save(employee);
        return employeeMapper.toDto(employee);
    }
}
