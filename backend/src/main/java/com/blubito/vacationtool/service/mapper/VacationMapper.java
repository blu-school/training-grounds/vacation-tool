package com.blubito.vacationtool.service.mapper;

import com.blubito.vacationtool.domain.Vacation;
import com.blubito.vacationtool.service.dto.VacationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface VacationMapper extends EntityMapper<VacationDTO, Vacation> {

    @Mapping(source = "employee.id", target = "employeeId")
    VacationDTO toDto(Vacation vacation);

    @Mapping(source = "employeeId", target = "employee.id")
    Vacation toEntity(VacationDTO vacationDTO);

    default Vacation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Vacation vacation = new Vacation();
        vacation.setId(id);
        return vacation;
    }
}
