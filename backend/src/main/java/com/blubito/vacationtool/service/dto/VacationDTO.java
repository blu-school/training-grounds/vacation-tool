package com.blubito.vacationtool.service.dto;

import com.blubito.vacationtool.domain.enumeration.VacationStatus;
import com.blubito.vacationtool.domain.enumeration.VacationType;
import lombok.Data;

import java.time.LocalDate;

/**
 * The type Vacation dto.
 */
@Data
public class VacationDTO {

    private Long id;

    private LocalDate startDate;

    private LocalDate endDate;

    private VacationStatus status;

    private Long employeeId;

    private VacationType type;

}
