package com.blubito.vacationtool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VacationToolApplication {

	public static void main(String[] args) {
		SpringApplication.run(VacationToolApplication.class, args);
	}

}
