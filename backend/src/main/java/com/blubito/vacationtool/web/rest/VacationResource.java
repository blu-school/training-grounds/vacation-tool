package com.blubito.vacationtool.web.rest;

import com.blubito.vacationtool.service.VacationService;
import com.blubito.vacationtool.service.dto.VacationDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.undertow.util.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * The type Vacation resource.
 */
@RestController
@RequestMapping("/api")
public class VacationResource {

    private final Logger log = LoggerFactory.getLogger(VacationResource.class);

    private final VacationService vacationService;

    /**
     * Instantiates a new Vacation resource.
     *
     * @param vacationService the vacation service
     */
    public VacationResource(VacationService vacationService) {
        this.vacationService = vacationService;
    }

    /**
     * Fetch all vacations.
     * <p>
     * TODO: Think about the depiction of your data. Fetch All means now with 300 entries as example that you have 300 results
     * Idea would be to use Pageable for setting for example 20 entries per page.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vacationDtos in body.
     */
    @Operation(summary = "Get a list of Vacations retrieved.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = VacationDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Failed to process response!"),
            @ApiResponse(responseCode = "401", description = "Unauthorized!"),
            @ApiResponse(responseCode = "403", description = "Forbidden!"),
            @ApiResponse(responseCode = "404", description = "Not Found!"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error!")})
    @GetMapping("/vacations")
    public ResponseEntity<List<VacationDTO>> fetchAllVacations() {
        log.debug("REST request to receive all Vacations");
        final List<VacationDTO> vacationDays = vacationService.getAllVacationDays();
        return ResponseEntity.ok().body(vacationDays);
    }

    /**
     * Creates a vacation.
     *
     * @param vacationDTO the vacation dto
     * @return the {@link ResponseEntity} with status {@code 201 (CREATED)} and the created employeeDto in body.
     * @throws BadRequestException the bad request exception
     */
    @Operation(summary = "Create a Vacation.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "CREATED", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = VacationDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Failed to process response!"),
            @ApiResponse(responseCode = "401", description = "Unauthorized!"),
            @ApiResponse(responseCode = "403", description = "Forbidden!"),
            @ApiResponse(responseCode = "404", description = "Not Found!"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error!")})
    @PostMapping("/vacations")
    public ResponseEntity<VacationDTO> createVacationEntry(@Valid @RequestBody VacationDTO vacationDTO) throws BadRequestException {
        log.debug("REST request to create Vacation {}", vacationDTO);
        if (vacationDTO.getId() != null) {
            throw new BadRequestException("New Vacation cannot contain id!"); //TODO: Should redesign exception handling in general.
        }
        VacationDTO result = vacationService.save(vacationDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }
}
