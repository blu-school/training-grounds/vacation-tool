package com.blubito.vacationtool.web.rest;

import com.blubito.vacationtool.service.EmployeeService;
import com.blubito.vacationtool.service.dto.EmployeeDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.undertow.util.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * The type Employee resource.
 */
@RestController
@RequestMapping("/api")
public class EmployeeResource {

    private final Logger log = LoggerFactory.getLogger(EmployeeResource.class);

    private final EmployeeService employeeService;

    /**
     * Instantiates a new Employee resource.
     *
     * @param employeeService the employee service
     */
    public EmployeeResource(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    /**
     * Fetch all employees.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of employeesDtos in body.
     */
    @Operation(summary = "Get a list of Employees retrieved.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = EmployeeDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Failed to process response!"),
            @ApiResponse(responseCode = "401", description = "Unauthorized!"),
            @ApiResponse(responseCode = "403", description = "Forbidden!"),
            @ApiResponse(responseCode = "404", description = "Not Found!"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error!")})
    @GetMapping("/employees")
    public ResponseEntity<List<EmployeeDTO>> fetchAllEmployees() {
        log.debug("REST fetchAllEmployees");
        List<EmployeeDTO> allEmployees = employeeService.getAllEmployees();
        return ResponseEntity.ok().body(allEmployees);
    }

    /**
     * Creates an employee.
     *
     * @param employeeDTO the employee dto
     * @return the {@link ResponseEntity} with status {@code 201 (CREATED)} and the created employeeDto in body.
     * @throws BadRequestException the bad request exception
     */
    @Operation(summary = "Create an Employee.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "CREATED", content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = EmployeeDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Failed to process response!"),
            @ApiResponse(responseCode = "401", description = "Unauthorized!"),
            @ApiResponse(responseCode = "403", description = "Forbidden!"),
            @ApiResponse(responseCode = "404", description = "Not Found!"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error!")})
    @PostMapping("/employees")
    public ResponseEntity<EmployeeDTO> createVacationEntry(@Valid @RequestBody EmployeeDTO employeeDTO) throws BadRequestException {
        log.debug("REST request to save employee {}", employeeDTO);
        if (employeeDTO.getId() != null) {
            throw new BadRequestException("New Employee cannot contain id!"); //TODO: Should redesign exception handling in general.
        }
        EmployeeDTO result = employeeService.save(employeeDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(result);
    }

}
