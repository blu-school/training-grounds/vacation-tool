/**
 * Package containing JPA domain objects.
 */
package com.blubito.vacationtool.domain;