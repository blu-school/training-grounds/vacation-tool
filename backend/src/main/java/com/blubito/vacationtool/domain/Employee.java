package com.blubito.vacationtool.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "employee")
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    private String firstName;

    private String middleName;

    private String lastName;

    private String personalNumber;

    private String blubitoAccId;

    @OneToMany(mappedBy="employee")
    private List<Vacation> vacations;

}
