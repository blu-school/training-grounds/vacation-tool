package com.blubito.vacationtool.domain;

import com.blubito.vacationtool.domain.enumeration.VacationStatus;
import com.blubito.vacationtool.domain.enumeration.VacationType;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "vacation")
public class Vacation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    private LocalDate startDate;

    private LocalDate endDate;

    private VacationStatus status;

    @ManyToOne(optional = false)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    private VacationType type;
}
