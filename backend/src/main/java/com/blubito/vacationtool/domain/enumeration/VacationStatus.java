package com.blubito.vacationtool.domain.enumeration;

public enum VacationStatus {
    PENDING, APPROVED, CANCELED, REJECTED
}
