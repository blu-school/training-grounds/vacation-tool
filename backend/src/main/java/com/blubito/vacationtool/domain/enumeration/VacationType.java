package com.blubito.vacationtool.domain.enumeration;

public enum VacationType {
    PAID, UNPAID, OTHER
}
