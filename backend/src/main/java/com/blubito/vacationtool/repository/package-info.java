/**
 * Package containing Spring Data JPA repositories.
 */
package com.blubito.vacationtool.repository;